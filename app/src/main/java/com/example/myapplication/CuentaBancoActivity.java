package com.example.myapplication;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class CuentaBancoActivity extends AppCompatActivity {
    private TextView lblNombre, lblSaldo;
    private EditText txtNumCuenta, txtNombre, txtBanco, txtSaldo,txtCantidad;
    private RadioGroup radioGroup;
    private RadioButton rdbDeposito,rdbRetiro,rdbConsulta;
    private Button btnRegistrar, btnRegresar, btnAplicar, btnLimpiar;

    private CuentaBanco cuenta;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cuenta_banco);
        iniciarComponentes();

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(valiEmpty()){
                    Toast.makeText(getApplicationContext(),"Por favor llene todos los campos", Toast.LENGTH_SHORT).show();
                }
                else{
                    cuenta.setNumCuenta(txtNumCuenta.getText().toString());
                    cuenta.setNombre(txtNombre.getText().toString());
                    cuenta.setBanco(txtBanco.getText().toString());
                    cuenta.setSaldo(Float.parseFloat(txtSaldo.getText().toString()));
                    rdbConsulta.setEnabled(true);
                    rdbRetiro.setEnabled(true);
                    rdbDeposito.setEnabled(true);
                    txtCantidad.setEnabled(true);
                    btnAplicar.setEnabled(true);
                }

            }
        });

        btnAplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(valiEmpty2()){
                    Toast.makeText(getApplicationContext(),"Por favor llene todos los campos", Toast.LENGTH_SHORT).show();
                }
                else{
                    float value;
                    if(valiRdb() == 1){
                        value = cuenta.hacerDeposito(Float.parseFloat(txtCantidad.getText().toString()));
                        cuenta.setSaldo(value);
                        lblSaldo.setText("Nuevo Saldo: $"+String.valueOf(cuenta.obtenerSaldo()));
                    } else if (valiRdb() == 2) {
                        lblSaldo.setText("Nuevo Saldo: $"+String.valueOf(cuenta.obtenerSaldo()));
                    } else if (valiRdb() == 3) {
                        value = cuenta.retirarDinero(Float.parseFloat(txtCantidad.getText().toString()));
                        if(value == -1){
                            Toast.makeText(getApplicationContext(),"No puede retirar dinero que no tiene", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            cuenta.setSaldo(value);
                            lblSaldo.setText("Nuevo Saldo: $"+String.valueOf(cuenta.obtenerSaldo()));
                        }

                    }
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtBanco.setText("");
                txtNombre.setText("");
                txtNumCuenta.setText("");
                txtSaldo.setText("");
                txtCantidad.setText("");
                lblSaldo.setText("Nuevo Saldo $");
                radioGroup.clearCheck();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public boolean valiEmpty(){
        if(txtNumCuenta.getText().toString().isEmpty() ||
        txtNombre.getText().toString().isEmpty() ||
        txtBanco.getText().toString().isEmpty() ||
        txtSaldo.getText().toString().isEmpty()){
            return true;
        }
        else{
            return false;
        }
    }

    public int valiRdb(){
        if(rdbDeposito.isChecked()){
            return 1;
        } else if (rdbConsulta.isChecked()) {
            return 2;
        } else if (rdbRetiro.isChecked()) {
            return 3;
        }
        else{
            return 0;
        }
    }
    public boolean valiEmpty2(){
        if(txtCantidad.getText().toString().isEmpty() || valiRdb() == 0){
            return true;
        }
        else{
            return false;
        }
    }



    public void iniciarComponentes(){
        lblNombre = findViewById(R.id.lblNombre);



        lblSaldo = findViewById(R.id.lblSaldo);

        txtNombre = (EditText) findViewById(R.id.txtNombreCliente);
        txtNumCuenta = (EditText) findViewById(R.id.txtNumeroCuenta);
        txtBanco = (EditText) findViewById(R.id.txtBanco);
        txtSaldo = (EditText) findViewById(R.id.txtSaldo);
        txtCantidad = findViewById(R.id.txtCantidad);

        rdbConsulta = findViewById(R.id.rdbConsultar);
        rdbRetiro = findViewById(R.id.rdbRetiro);
        rdbDeposito = findViewById(R.id.rdbDeposito);

        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);
        btnAplicar = (Button) findViewById(R.id.btnAplicarMovimiento);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        this.cuenta = new CuentaBanco();

        Bundle datos = getIntent().getExtras();

        assert datos != null;

        lblNombre.setText(datos.getString("nombre"));


    }


}